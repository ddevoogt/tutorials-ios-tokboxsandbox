# README #

Set up a Basic iOS Client (Swift)
This tutorial will walk you through the steps of setting up a basic iOS client that uses OpenTok.

Overview

All OpenTok applications require both a client and a server component. The client-side code is what loads in an end-user's iOS device and handles the majority of OpenTok functionality, including connecting to the session, publishing audio-video streams to the session, and subscribing to other clientsâ€™ streams. For more information on clients, servers, and sessions, see OpenTok Basics.

In this tutorial, you will be utilizing the OpenTok iOS SDK, OpenTok's client-side library for iOS devices, to quickly and easily build a real-time interactive video application.

Note: The code in this tutorial is written in Swift. There is also a tutorial with the code written in Objective-C.

URL: https://tokbox.com/developer/tutorials/ios/swift/basic-video-chat/

# Chapters:

* https://tokbox.com/developer/tutorials/ios/swift/basic-video-chat/#requirements
* https://tokbox.com/developer/tutorials/ios/swift/basic-video-chat/#creating-a-new-project
* https://tokbox.com/developer/tutorials/ios/swift/basic-video-chat/#adding-the-opentok-library
* https://tokbox.com/developer/tutorials/ios/swift/basic-video-chat/#authentication             
* https://tokbox.com/developer/tutorials/ios/swift/basic-video-chat/#connect  
* https://tokbox.com/developer/tutorials/ios/swift/basic-video-chat/#publish    
* https://tokbox.com/developer/tutorials/ios/swift/basic-video-chat/#subscribe      <================
* https://tokbox.com/developer/tutorials/ios/swift/basic-video-chat/#run
* https://tokbox.com/developer/tutorials/ios/swift/basic-video-chat/#next
* https://tokbox.com/developer/tutorials/ios/swift/basic-video-chat/#server


# Tokbok account
https://tokbox.com/account/#/project/45947912

# Tokbox video playground
https://tokbox.com/developer/tools/playground/?v=2.11&session_id=1_MX40NTk0NzkxMn5-MTUwNDAyNjEwNjg3NX5vVndra2grcGtHWkNQNGpCQytCTjhCSXJ-fg&token=T1%3D%3DcGFydG5lcl9pZD00NTk0NzkxMiZzaWc9MWMxZjUwYzEyMDE0ZjZmNTNhYzQ5ZTI2ZmViNjU3ZWQ3NWNkZTA2NzpzZXNzaW9uX2lkPTFfTVg0ME5UazBOemt4TW41LU1UVXdOREF5TmpFd05qZzNOWDV2Vm5kcmEyZ3JjR3RIV2tOUU5HcENReXRDVGpoQ1NYSi1mZyZjcmVhdGVfdGltZT0xNTA0MDQyNDkwJm5vbmNlPTAuMDY2NzYwNzM3MDA3NTQ2NDgmcm9sZT1wdWJsaXNoZXImZXhwaXJlX3RpbWU9MTUwNjYzNDQ4OSZpbml0aWFsX2xheW91dF9jbGFzc19saXN0PQ%3D%3D
