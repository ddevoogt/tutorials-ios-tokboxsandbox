//
//  ViewController.swift
//  TokboxSandbox
//
//  Created by Douglas De Voogt on 8/29/17.
//  Copyright © 2017 Douglas. All rights reserved.
//

import UIKit
import OpenTok

// Replace with your OpenTok API key
var kApiKey = "45947912"
// Replace with your generated session ID
var kSessionId = "1_MX40NTk0NzkxMn5-MTUwNDAyNjEwNjg3NX5vVndra2grcGtHWkNQNGpCQytCTjhCSXJ-fg"
// Replace with your generated token
var kToken = "T1==cGFydG5lcl9pZD00NTk0NzkxMiZzaWc9MWMxZjUwYzEyMDE0ZjZmNTNhYzQ5ZTI2ZmViNjU3ZWQ3NWNkZTA2NzpzZXNzaW9uX2lkPTFfTVg0ME5UazBOemt4TW41LU1UVXdOREF5TmpFd05qZzNOWDV2Vm5kcmEyZ3JjR3RIV2tOUU5HcENReXRDVGpoQ1NYSi1mZyZjcmVhdGVfdGltZT0xNTA0MDQyNDkwJm5vbmNlPTAuMDY2NzYwNzM3MDA3NTQ2NDgmcm9sZT1wdWJsaXNoZXImZXhwaXJlX3RpbWU9MTUwNjYzNDQ4OSZpbml0aWFsX2xheW91dF9jbGFzc19saXN0PQ=="

class ViewController: UIViewController {
    var session: OTSession?
    var publisher: OTPublisher?
    var subcriber: OTSubscriber?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        connectToAnOpenTokSession()
    }
    
    func connectToAnOpenTokSession() {
        session = OTSession(apiKey: kApiKey, sessionId: kSessionId, delegate: self)
        var error: OTError?
        session?.connect(withToken: kToken, error: &error)
        if error != nil {
            print(error!)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

// MARK: - OTSessionDelegate callbacks
extension ViewController: OTSessionDelegate {
    func sessionDidConnect(_ session: OTSession) {
        print("The client connected to the OpenTok session.")
        
        let settings = OTPublisherSettings()
        settings.name = UIDevice.current.name
        guard let publisher = OTPublisher(delegate: self, settings: settings) else {
            return
        }
        
        var error: OTError?
        session.publish(publisher, error: &error)
        guard error == nil else {
            print(error!)
            return
        }
        
        guard let publisherView = publisher.view else {
            return
        }
        
        let screenBounds = UIScreen.main.bounds
        publisherView.frame = CGRect(x: screenBounds.width - 150 - 20, y: screenBounds.height - 150 - 20, width: 150, height: 150)
        view.addSubview(publisherView)
    }
    
    func sessionDidDisconnect(_ session: OTSession) {
        print("The client disconnected from the OpenTok session.")
    }
    
    func session(_ session: OTSession, didFailWithError error: OTError) {
        print("The client failed to connect to the OpenTok session: \(error).")
    }
    func session(_ session: OTSession, streamCreated stream: OTStream) {
        print("A stream was created in the session.")
        subcriber = OTSubscriber(stream: stream, delegate: self)
        guard let subcriber = subcriber else {
            print("guard let subcriber = subcriber")
            return
        }
        
        var error: OTError?
        session.subscribe(subcriber, error: &error)
        guard error == nil else {
            print("guard error == nil")
            print(error!)
            return
        }
        
        guard let subscriberView = subcriber.view else {
            print("let subscriberView = subcriber.view")
            return
        }
        subscriberView.frame = UIScreen.main.bounds
        view.insertSubview(subscriberView, at: 0)
        print("end session(_ session: OTSession, streamCreated stream: OTStream)")
    }
    
    func session(_ session: OTSession, streamDestroyed stream: OTStream) {
        print("A stream was destroyed in the session.")
    }
}

// MARK: - OTPublisherDelegate callbacks
extension ViewController: OTPublisherDelegate {
    func publisher(_ publisher: OTPublisherKit, didFailWithError error: OTError) {
        print("The publisher failed: \(error)")
    }
}

// Mark: - OTSubcriberDelegate
extension ViewController: OTSubscriberDelegate {
    public func subscriberDidConnect(toStream subscriber: OTSubscriberKit) {
        print("The subscriber did connect to the stream.")
    }
    
    public func subscriber(_ subscriber: OTSubscriberKit, didFailWithError error: OTError) {
        print("The subscriber failed to connect to the stream.")
    }
}

